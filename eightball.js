
(function ($) {

  Drupal.behaviors.eightballAnswer = {
    attach: function(context, settings) {
      $("#block-eightball-eightball").click(function() {
        var eightballUrl = settings.basePath + 'eightball';
        $.get(eightballUrl, function(data){
          $("#block-eightball-eightball").fadeOut('slow', function(){
            $("#block-eightball-eightball", context).html(data).fadeIn('slow', function(){
              $("#block-eightball-eightball").delay(3000).fadeOut('slow', function(){
                $("#block-eightball-eightball").html('<span onclick=eightballAnswer()><img src="' + settings.basePath + 'sites/all/modules/contrib/eightball/eightball1.png" /></span>').fadeIn('slow');
              });
            });  
          });          
        });
      });
    }
  }

})(jQuery);
